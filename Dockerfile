ARG ES_VERSION=7.1.1

FROM docker.elastic.co/elasticsearch/elasticsearch:${ES_VERSION}

# Install mapper-attachments (https://www.elastic.co/guide/en/elasticsearch/plugins/current/ingest-attachment.html)
RUN /usr/share/elasticsearch/bin/elasticsearch-plugin install --batch ingest-attachment

# Install s3 snapshot plugin
RUN /usr/share/elasticsearch/bin/elasticsearch-plugin install --batch repository-s3

# Export HTTP & Transport
EXPOSE 9200 9300
